export type Recipe = {
  id: string; // idMeal
  title: string; // - strMeal
  ingredient: Array<{ingredient: string, mesure: string}>; // to convert en (ingredient,mesure)[] - (strIngredient, strMeasure)
  //measure: string[]; // strMeasure
  Category: string[]; // to convert "Beef,pasta" -> ["Beef","pasta"]  -strCategory
  Tags: string[]; // // to convert "Beef,pasta" -> ["Beef","pasta"] - strTags
  instruction: string; // - strInstructions
  picture?: string; // strMealThumb
  area: string; // - strArea
}