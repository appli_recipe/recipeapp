import React from 'react'
import { FlatList, Image, StyleSheet, Text, View } from 'react-native';
import { Recipe } from '../types';

type Props = {
  recipe: Recipe;
}

const RecipeItem: React.FC<Props> = ({ recipe }) => {
	return (
		<View style={styles.container}>
			<View style={styles.imageContainer}>
				<Image source={{ uri: recipe.picture }} style={styles.image} resizeMode='contain' />
			</View>
			<View style={styles.infosContainer}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode='tail'>{recipe.title}</Text>
        <Text style={styles.category} numberOfLines={1}>{recipe.Category?.toString()}</Text>
        <View style={styles.containerTag}>
             {recipe.Tags?.map((item) => {
                return <Text style={styles.tag}>{item}</Text>
             })}
        </View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderRadius: 10,
		paddingHorizontal: 16,
		paddingVertical: 16,
		display: "flex",
		flexDirection: "column",
		borderBottomWidth: 2,
    borderBottomColor: "#E9E9E9",
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1
  },
  containerTag: {
    display: "flex",
    flexDrection: "row"
  },
	imageContainer: {
		width: 90,
		display: "flex",
		justifyContent: "center",
		alignItems: 'center'
	},
	image: {
		height: 120,
		aspectRatio: 1,
	},
	infosContainer: {
		flex: 1,
		paddingHorizontal: 16,
	},
	title: {
		fontSize: 18,
		fontWeight: '700',
	},
  category: {
    fontSize: 10,
  },
  tag: {
    fontSize: 9,
    backgroundColor: "#6DE45D",
    borderColor: "#0C5202",
    borderWidth: 2,
    width: 50,
    textAlign: 'center',
    borderRadius: 20
  },
  ingredients: {
    flex: 1
  },
  instructions: {
    flex: 1
  }
});

export default RecipeItem