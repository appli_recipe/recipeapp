import {Recipe} from './src/types';

export const ToRecipe = (data : any) => {
  const ingredients: Array<{ingredient: string, mesure: string}> = [];
  //const measures: string[] = [];
  for(let i=1; i<21; i++) {
      /*  
      if (data[`strIngredient${i}`] != null && data[`strIngredient${i}`].length > 0) 
            ingredients.push(data[`strIngredient${i}`])
        if(data[`strMeasure${i}`] != null && data[`strMeasure${i}`].length>0) 
            measures.push(data[`strMeasure${i}`])
            */
    if (data[`strIngredient${i}`] != null && data[`strIngredient${i}`].length > 0) {
        ingredients.push({ ingredient: data[`strIngredient${i}`], mesure: data[`strMeasure${i}`] });
      }
    }
    const recipe: Recipe = {
        id: data.idMeal,
        title: data.strMeal,
        instruction: data.strInstructions,
        picture: data.strMealThumb,
        area: data.strArea,
        ingredient: ingredients,
        //measure: measures,
        Category: data.strCategory?.split(','),
        Tags: data.strTags?.split(',')
    }
    return recipe
}