import React, { useEffect } from 'react';
import axios from 'axios';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { Text, View, StyleSheet, SafeAreaView, Platform, TouchableOpacity } from 'react-native';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { MainStackParamsList } from '../../App';
import { Recipe } from '../types';
import { ToRecipe } from '../Utils';

type Props = {
    navigation: StackNavigationProp<MainStackParamsList, "Home">
}

const homeScreen: React.FC<Props> = ({ navigation }) => {

  const onPressSearch = () => {
    navigation.navigate("Search")
  }
  const onPressFavorite = () => {
    navigation.navigate("Favorite")
  }

  const onPressRandom = async () => {
    const res = await axios.get(`https://www.themealdb.com/api/json/v1/1/random.php`)
    console.log(res.data.meals)
    const recipe: Recipe = ToRecipe(res.data.meals[0])
    navigation.navigate("Details", { recipe: recipe})
  }

  return (
      <SafeAreaView style={styles.droidSafeArea} >
        <Text style={styles.title}>Recipe App</Text>
        <View style={styles.container}>
          <TouchableOpacity style={styles.section} onPress={onPressSearch}>
            <FontAwesome name="search" size={24} color="black" />
            <Text>Search</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.section} onPress={onPressFavorite}>
            <MaterialIcons name="favorite" size={24} color="black" />
            <Text>Favorite</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.section} onPress={onPressRandom}>
            <FontAwesome name="random" size={24} color="black" />
            <Text>Random</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
  );
};

export default homeScreen;

const styles = StyleSheet.create({
  droidSafeArea:{
    flex: 1,
    width:"80%",
    paddingTop: Platform.OS === 'android'? 25:0
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  title:{
    fontSize: 18,
	fontWeight: '700',
	color: "#2413A2",
  },
  section: {
    flex: 1
  }
});
