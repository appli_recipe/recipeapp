import * as React from 'react';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { Text, View, StyleSheet, SafeAreaView, Platform, StatusBar } from 'react-native';
import { MainStackParamsList } from '../../App';

type Props = {
  navigation: StackNavigationProp<MainStackParamsList>
}

const FavoriteScreen: React.FC<Props> = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="dark" />
      <SafeAreaView style={styles.droidSafeArea}>
        <Text>Favorite</Text>
      </SafeAreaView>
    </View>
  );
};

export default FavoriteScreen;

const styles = StyleSheet.create({
  droidSafeArea:{
    flex: 1,
    paddingTop: Platform.OS === 'android'? 25:0
  },
  container: {
      flexDirection: "row"
  },
  section: {
    flex: 1
  }
});
