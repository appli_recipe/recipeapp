import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { Text, View, StyleSheet, SafeAreaView, Platform, StatusBar, TextInput,FlatList, TouchableOpacity } from 'react-native';
import { MainStackParamsList } from '../../App';
import { Recipe } from '../types';
import RecipeItem from '../components/RecipeItem';
import { ToRecipe } from '../Utils';

type Props = {
  navigation: StackNavigationProp<MainStackParamsList, "Search">
}

const SearchScreen: React.FC<Props> = ({ navigation }) => {

  const [query, setQuery] = useState<string>('')
  const [recipes, setRecipes] = useState<Recipe[]>([])
  const [favoriteList, setFavoriteList] = useState<string[]>([])

  useEffect(() => {
    AsyncStorage.getItem('favoriteList')
			.then(value => {
				if (!value)
					return
          setFavoriteList(JSON.parse(value))
			})
		console.log("component mounted")
	}, [])

  const searchRecipe = async () => {
    const res = await axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?s=${query}`)
    setRecipes(res.data.meals?.map((item: any ) => ToRecipe(item)))
  }

  const onPressRecipe = ( item : Recipe ) => {
    navigation.navigate("Details", { recipe : item })
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="dark" />
      <SafeAreaView style={styles.droidSafeArea}>
        <TextInput style={styles.input} value={query} onChangeText={setQuery}/>
        <TouchableOpacity style={styles.buttonSearch} onPress={searchRecipe}>
          <Text style={styles.textButtonSearch}> Search </Text>
        </TouchableOpacity>


        <FlatList
            data={recipes}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => onPressRecipe(item)}>
                <RecipeItem recipe={item}/>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <View>
                <Text style={styles.emptylist}>Search recipes</Text>
              </View>
            }
            keyExtractor={item => item.id}
        />
      </SafeAreaView>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  droidSafeArea:{
    flex: 1,
    paddingTop: Platform.OS === 'android'? 25:0
  },
  emptylist:{
      display: "flex",
      height:"100%",
    color: '#CACACA',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
      flexDirection: "row"
  },
  buttonSearch: {
    height:30,
    width: "80%",
    borderRadius:5,
    marginRight: "auto",
    marginLeft: "auto",
    marginTop: 5,
    marginBottom: 5,
    alignItems:"center",
    fontWeight: 'bold',
    backgroundColor: "#76A5DE"
  },
  textButtonSearch:{
    textAlign: "center",
    color:"white",
    fontSize: 20,
  },
  input: {
    fontSize: 22,
    borderWidth: 1,
    borderColor: 'grey'
  }
});
