import * as React from 'react';
import { NavigationContainer, RouteProp, StackActions } from '@react-navigation/native';
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import { Text, View, StyleSheet, SafeAreaView, Platform, FlatList, Image, StatusBar } from 'react-native';
import { MainStackParamsList } from '../../App';
import { Recipe } from '../types';
import { SimpleLineIcons, Fontisto } from '@expo/vector-icons';

type Props = {
  navigation: StackNavigationProp<MainStackParamsList, "Details">
  route: RouteProp<MainStackParamsList, "Details">
//   recipe: Recipe
}

const DetailsScreen: React.FC<Props> = ({ navigation, route }) => {
  
  const { recipe } = route.params
  navigation.setOptions({title: recipe.title})
  
  const isFavorite = (id: string) => {
    // TODO
    return true
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="dark" />
      <SafeAreaView style={styles.droidSafeArea}>
        <View style={styles.imageContainer}>
          <Image source={{ uri: recipe.picture }} style={styles.image} resizeMode='contain' />
        </View>
        <View style={styles.infosContainer}>
          <Text style={styles.title} ellipsizeMode='tail'>{recipe.title}</Text>
          
          {/* if(isFavorite(recipe.id)) <Fontisto name="heart" size={24} color="black" />
          else <SimpleLineIcons name="heart" size={24} color="black" /> */}
          <Text style={styles.category}>{recipe.Category.toString()}</Text>
          <View style={styles.containerTag}>
             {recipe.Tags?.map((item) => {
                return <Text style={styles.tag}>{item}</Text>
             })}
          </View>
          <View style={styles.containerItem}>
            <Text style={styles.subtitle}>Ingrédients :</Text>
            <FlatList
              data={recipe.ingredient}
              renderItem={({item}) => {
                  return <Text> • {item.ingredient} : {item.mesure}</Text>
                }}
              keyExtractor={item => item.ingredient}
            />
          </View>
          <View style={styles.containerItem}>
          <Text style={styles.subtitle}>Instructions :</Text>
            <Text>{recipe.instruction}</Text>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default DetailsScreen;

const styles = StyleSheet.create({
  droidSafeArea:{
    flex: 1,
    paddingTop: Platform.OS === 'android'? 25:0
  },
  container: {
      flexDirection: "row"
  },
  containerTag: {
    display: "flex",
    flexDrection: "row"
  },
  section: {
    flex: 1
  },
  imageContainer: {
		width: '100%',
		display: "flex",
		justifyContent: "center",
		alignItems: 'center'
	},
  image: {
		height: 120,
		aspectRatio: 1,
	},
	infosContainer: {
		flex: 1,
		paddingHorizontal: 16,
	},
	title: {
		fontSize: 18,
		fontWeight: '700',
  },
  subtitle: {
    fontSize: 16,
    fontWeight: '500',
    marginTop: 20
  },
  category: {
    fontSize: 10,
  },
  tag: {
    fontSize: 9,
    backgroundColor: "#6DE45D",
    borderColor: "#0C5202",
    borderWidth: 2,
    width: 50,
    textAlign: 'center',
    borderRadius: 20
  },
  containerItem: {
    flex: 1,
    marginTop: 5
  }
});
